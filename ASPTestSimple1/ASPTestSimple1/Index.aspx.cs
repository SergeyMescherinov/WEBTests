﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPTestSimple1
{
    public partial class Index : System.Web.UI.Page
    {
        

        public void ShowLineage()
        {
            Response.Write("Check out the family tree: <br/><br/>");
            Response.Write(this.GetType().ToString());

            Type baseObject = this.GetType().BaseType;

            while (!baseObject.Equals(typeof(object)))
            {
                Response.Write(" derives from: <br/><br/>");
                Response.Write(baseObject.ToString());
                baseObject = baseObject.BaseType;
            }
        }
    }
}