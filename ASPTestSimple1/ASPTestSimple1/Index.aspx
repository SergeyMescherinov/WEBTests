﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="ASPTestSimple1.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<head runat="server">
    <title>Test title</title>
</head>
<body>
    <script runat="server">

        protected void Page_Load(object sender, EventArgs args)
        {
            ddl.Items.Add("item 1");
            ddl.Items.Add("item 2");
            ddl.Items.Add("item 3");
            ddl.Items.Add("item 4");
        }
    </script>


    <form method="post" action="Index.aspx" id="form1" runat="server">

        <asp:Label Text="type in me" runat="server"></asp:Label>
        <asp:TextBox ID="textInfo" runat="server"></asp:TextBox>
        <br />
        <asp:DropDownList ID="ddl" runat="server">
        </asp:DropDownList>
        <asp:Button ID="clickme" Text="Click me!" runat="server" />
        <p>
            <% if (Request.Params["textInfo"] != "")
                { %>
           This was in the text box: <%= Request.Params["textInfo"] %>

            <% } %>
        </p>
    </form>
</html>

